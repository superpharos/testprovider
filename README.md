# testprovider Project

> RC3 Build 57

#### An TestExam Creator and taker with order built with  Django REST Framework

Full documentation: http://coming.soon
* But you can and browse our built in API documents in :
* http://localhost/doc/
* http://localhost/redoc/
* http://localhost/swagger/


## Installation

1. `pip install requirements.txt`

2. Download `Project files` to your `DRF root` location:

3. By default docs are public but you can
 override default permissions by 
 removing `permission_classes`' urls.py in testprovider root dir:
 
    ```python
        url(r'^docs/', include_docs_urls(title='TestExam API Doc', public=True,
                                     permission_classes=[permissions.AllowAny]))
    ```
    
## Requirements
* Django 1.11+
* Django REST framework 3.8+
* Python  3.5, 3.6, 3.7

## Special Thanks
Many thanks to Tom Christie & all the contributors who have developed [Django REST Framework](http://django-rest-framework.org/)
* Also My Mentors who taught me a lot of programming 
