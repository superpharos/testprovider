from django.contrib import admin

from .models import Category, TestExam, Question, Choice


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'title')


@admin.register(TestExam)
class TestExamAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'category', 'title',
                    'created_time', 'updated_time', 'price_unit')
    list_filter = ('user', 'category', 'created_time',
                   'updated_time', 'price_unit')

    def get_readonly_fields(self, request, obj=None):
        if not obj:
            return []
        if request.user.is_superuser or obj.creator == request.creator:
            return []
        else:
            return list(set(
                [field.name for field in self.opts.local_fields] +
                [field.name for field in self.opts.local_many_to_many]
            ))


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ('id', 'test_exam', 'question_number', 'created_time')
    list_filter = ('test_exam', 'question_number', 'created_time',)

    def get_readonly_fields(self, request, obj=None):
        if not obj:
            return []
        if request.user.is_superuser or obj.creator == request.creator:
            return []
        else:
            return list(set(
                [field.name for field in self.opts.local_fields] +
                [field.name for field in self.opts.local_many_to_many]
            ))


@admin.register(Choice)
class ChoiceAdmin(admin.ModelAdmin):
    list_display = ('id', 'question', 'choice_number', 'created_time')
    list_filter = ('question', 'choice_number', 'created_time',)

    def get_readonly_fields(self, request, obj=None):
        if not obj:
            return []
        if request.user.is_superuser or obj.creator == request.creator:
            return []
        else:
            return list(set(
                [field.name for field in self.opts.local_fields] +
                [field.name for field in self.opts.local_many_to_many]
            ))
