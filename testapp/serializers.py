from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.contrib.auth.models import User

from .models import TestExam, Question, Choice


class ChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = ('id', 'question', 'content', 'choice_number', 'image')


class ChoiceAbstractSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = ('id', 'content', 'image')


class QuestionSerializer(serializers.ModelSerializer):
    choices = ChoiceSerializer(many=True)

    class Meta:
        model = Question
        fields = ('id', 'test_exam', 'content',
                  'question_number', 'choices', 'image')


class TestExamSerializer(serializers.ModelSerializer):
    questions = QuestionSerializer(many=True)

    class Meta:
        model = TestExam
        fields = ('id', 'title', 'description', 'image', 'questions')


class TestExamAbstractSerializer(serializers.ModelSerializer):

    class Meta:
        model = TestExam
        fields = ('id', 'title', 'description')


class UserRegistrationSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    email = serializers.EmailField(
            required=True,
            validators=[UniqueValidator(queryset=User.objects.all())]
            )
    username = serializers.CharField(
            max_length=32,
            validators=[UniqueValidator(queryset=User.objects.all())]
            )
    password = serializers.CharField(min_length=6,
                                     max_length=100, write_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password',
                  'first_name', 'last_name')

    def get_fields(self):
        fields = super().get_fields()
        fields['password2'] = serializers.CharField(write_only=True)
        return fields

    def validate(self, data):
        password = data.get('password')
        confirm_password = data.pop('password2')
        if password != confirm_password:
            raise serializers.ValidationError("Those passwords don't match.")
        return data

    def create(self, validated_data):
        print(validated_data)
        user = User(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user
