import json

from django.contrib.auth.models import User
from django.urls import reverse

from rest_framework.test import APITestCase


class UserRegistrationAPIViewTestCase(APITestCase):
    url = reverse("user_registration")

    def test_invalid_password(self):
        """
        Test to verify that a post call with invalid passwords
        """
        user_data = {
            "username": "testuser",
            "email": "test@localhost",
            "password": "password",
            "password2": "INVALID_PASSWORD"
        }
        response = self.client.post(self.url, user_data)
        self.assertEqual(400, response.status_code)

    def test_user_registration(self):
        """
        Test to verify that a post call with user valid data
        """
        user_data = {
            "username": "testuser",
            "email": "test@localhost",
            "password": "123123",
            "password2": "123123"
        }
        response = self.client.post(self.url, user_data)
        self.assertEqual(201, response.status_code)
        self.assertTrue("token" in json.loads(response.content))

    def test_unique_username_validation(self):
        """
        Test to verify that a post call with already exists username
        """
        user_data_1 = {
            "username": "testuser",
            "email": "test@localhost",
            "password": "123123",
            "password2": "123123"
        }
        response = self.client.post(self.url, user_data_1)
        self.assertEqual(201, response.status_code)

        user_data_2 = {
            "username": "testuser",
            "email": "test2@localhost",
            "password": "123123",
            "password2": "123123"
        }
        response = self.client.post(self.url, user_data_2)
        self.assertEqual(400, response.status_code)


class UserLoginAPIViewTestCase(APITestCase):
    url = reverse("rest_login")

    def setUp(self):
        self.username = "arash"
        self.email = "arash@localhost"
        self.password = "Arash is valid"
        self.user = User.objects.create_user(self.username,
                                             self.email, self.password)

    def test_authentication_without_password(self):
        response = self.client.post(self.url, {"username": "Arash"})
        self.assertEqual(400, response.status_code)

    def test_authentication_with_wrong_password(self):
        response = self.client.post(self.url, {"username": self.username,
                                               "password": "Arash is valid?"})
        self.assertEqual(400, response.status_code)

    def test_authentication_with_valid_data(self):
        response = self.client.post(self.url, {"username": self.username,
                                               "password": self.password})
        self.assertEqual(200, response.status_code)
        self.assertTrue("key" in json.loads(response.content))



