from django.conf.urls import url
from testapp.views import RegisterView, TestExamViewSet
from rest_auth.views import (
    LoginView, LogoutView, UserDetailsView, PasswordChangeView,
    PasswordResetView, PasswordResetConfirmView
)

urlpatterns = [
    url(r'^api/v1/tests/$', TestExamViewSet.as_view({'get': 'list'}),
        name='test'),
    url(r'^api/v1/tests/(?P<pk>[0-9]+)/$', TestExamViewSet.as_view({'get': 'retrieve'}),
        name='test2'),
    url(r'^api/v1/password/reset/$', PasswordResetView.as_view(),
        name='rest_password_reset'),
    url(r'^api/v1//password/reset/confirm/$', PasswordResetConfirmView.as_view(),
        name='rest_password_reset_confirm'),
    url(r'^api/v1/login/$', LoginView.as_view(), name='rest_login'),
    url(r'^api/v1/logout/$', LogoutView.as_view(), name='rest_logout'),
    url(r'^api/v1/user/$', UserDetailsView.as_view(), name='rest_user_details'),
    url(r'^api/v1/password/change/$', PasswordChangeView.as_view(),
        name='rest_password_change'),
    url(r'^api/v1/register/$', RegisterView.as_view(),
        name='user_registration'),
]
