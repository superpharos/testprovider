from django.db import models
from django.db.models import SET_NULL
from django.utils.translation import ugettext_lazy as _
from django.conf import settings


class Category(models.Model):
    title = models.CharField(max_length=200, unique=True, verbose_name=_("title"))
    description = models.TextField(blank=True, null=True,
                                   verbose_name=_("description"))
    image = models.ImageField(upload_to='categories/%Y/%m/%d',
                              blank=True, null=True)

    def __str__(self):
        return '%s' % (self.title,)


class TestExam(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             verbose_name=_("Test Creator"))
    category = models.ForeignKey(Category, related_name='test_exams',
                                 verbose_name=_("Categories"), blank=True,
                                 null=True, on_delete=SET_NULL
                                 )
    title = models.CharField(max_length=200, verbose_name=_("title"))
    description = models.TextField(blank=True, null=True,
                                   verbose_name=_("description"))
    created_time = models.DateTimeField(auto_now_add=True,
                                        verbose_name=_("created time"))
    updated_time = models.DateTimeField(auto_now=True, null=True,
                                        verbose_name=_("updated time"))
    price_unit = models.PositiveIntegerField(verbose_name=_("price per unit"))
    image = models.ImageField(upload_to='test_exams/%Y/%m/%d',
                              blank=True, null=True)

    def __str__(self):
        return '%s by %s' % (self.title, self.user)


class Question(models.Model):
    test_exam = models.ForeignKey(TestExam, related_name='questions',
                                  verbose_name=_("Test Exam"))
    content = models.CharField(max_length=200, verbose_name=_("content"))
    created_time = models.DateTimeField(auto_now_add=True,
                                        verbose_name=_("created time"))
    question_number = models.PositiveIntegerField(verbose_name=_("question number"))
    image = models.ImageField(upload_to='questions/%Y/%m/%d', blank=True, null=True)

    class Meta:
        ordering = ['question_number']

    def __str__(self):
        return '%s' % (self.id,)


class Choice(models.Model):
    question = models.ForeignKey(Question, related_name='choices',
                                 verbose_name=_("Question"))
    content = models.CharField(max_length=200, verbose_name=_("content"))
    created_time = models.DateTimeField(auto_now_add=True,
                                        verbose_name=_("created time"))
    choice_number = models.PositiveIntegerField(verbose_name=_("choice number"))
    image = models.ImageField(upload_to='choices/%Y/%m/%d', blank=True, null=True)

    class Meta:
        ordering = ['choice_number']

    def __str__(self):
        return '%s' % (self.id,)


