from rest_framework import permissions


class IsUser(permissions.BasePermission):
    """
    Custom permission to disallow admin and staff to access.
    """
    def has_permission(self, request, view):
        return request.user.is_authenticated and not request.user.is_staff\
               and not request.user.is_superuser
