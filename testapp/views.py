import coreapi
import coreschema
from drf_yasg.utils import swagger_auto_schema
from rest_framework import schemas
from rest_framework.views import APIView
# from rest_framework.generics import CreateAPIView
from rest_framework.viewsets import ModelViewSet
# from django.contrib.auth import authenticate
# from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
# from rest_framework.decorators import permission_classes, api_view
from rest_framework import permissions
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_201_CREATED
)
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from django_filters import FilterSet, CharFilter, NumberFilter

from .models import TestExam, Question
from .serializers import TestExamAbstractSerializer, UserRegistrationSerializer,\
    TestExamSerializer, QuestionSerializer


class RegisterView(APIView):
    """
       Register New users.
    """
    schema = schemas.ManualSchema(fields=[
        coreapi.Field(
            "username",
            required=True,
            location="path",
            schema=coreschema.String(description='Unique username'),
        ),
        coreapi.Field(
            "email ",
            required=True,
            location="path",
            schema=coreschema.String(description='Unique Email')
        ),
        coreapi.Field(
            "password1",
            required=True,
            location="path",
            schema=coreschema.String(description='Strong Password Please')
        ),
        coreapi.Field(
            "password2",
            required=True,
            location="path",
            schema=coreschema.String(description='Confirm password')
        ),
        coreapi.Field(
            "first_name",
            required=False,
            location="path",
            schema=coreschema.String(description='Optional Field')
        ),
        coreapi.Field(
            "last_name",
            required=False,
            location="path",
            schema=coreschema.String(description='Optional Field')
        ),
    ])

    permission_classes = (permissions.AllowAny,)

    @swagger_auto_schema(request_body=UserRegistrationSerializer)
    def post(self, request, format='json'):
        serializer = UserRegistrationSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                token = Token.objects.create(user=user)
                json = serializer.data
                json['token'] = token.key
                return Response(json, status=HTTP_201_CREATED)

        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class TestExamFilter(FilterSet):
    price_unit = NumberFilter(field_name='price_unit', lookup_expr='exact')
    price_unit__gt = NumberFilter(field_name='price_unit', lookup_expr='gt')
    price_unit__lt = NumberFilter(field_name='price_unit', lookup_expr='lt')

    class Meta:
        model = TestExam
        fields = {
            'created_time': ['exact', 'year__gt', 'year__lt', 'month__gt',
                             'month__lt', 'day__gt', 'day__lt'],
            'category__title': ['exact'],
        }


class TestExamViewSet(ModelViewSet):
    """
        retrieve:
        Return the given TestExam.

        list:
        Return a list of all the existing TestExams.
    """
    # schema = schemas.AutoSchema(manual_fields=[
    #     coreapi.Field(
    #         "created_time",
    #         required=True,
    #         location="query",
    #         schema=coreschema.String(description='created_time'),
    #     ),
    # ])

    serializer_class = TestExamSerializer
    queryset = TestExam.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filter_class = TestExamFilter
    http_method_names = ['get']
