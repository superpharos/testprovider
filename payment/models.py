from django.db import models
from django.db.models import PROTECT
from django.utils.translation import ugettext_lazy as _

from order.models import Order, OrderTestExam
from testapp.models import Category
from testtaker.models import TestTaker


class Payment(models.Model):
    amount = models.PositiveIntegerField(default=0,
                                         verbose_name=_("Total Price"))
    created_time = models.DateTimeField(auto_now_add=True,
                                        verbose_name=_("created time"))
    payment_status = models.BooleanField(default=False,
                                         verbose_name=_('Is Paid?'))
    order = models.OneToOneField(Order, related_name='payments',
                                 verbose_name=_('Order ID'), on_delete=PROTECT)

    def save(self, *args, **kw):
        if self.pk is not None:
            original = Payment.objects.get(pk=self.pk)
            if original.payment_status != self.payment_status:
                for each in OrderTestExam.objects.filter(order_id=self.order):
                    quantity = each.quantity
                    test_exam = each.test_exam
                    while quantity > 0:
                        TestTaker.objects.create(order_id=self.order.id, test_exam=test_exam)
                        quantity -= 1
        super(Payment, self).save(*args, **kw)

    def __str__(self):
        return '%s - %s' % (self.id, self.payment_status)
