from django.contrib import admin

from .models import Payment


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    list_display = ('id', 'order', 'amount', 'payment_status', 'created_time')
    list_filter = ('order', 'payment_status', 'created_time',)

    def get_readonly_fields(self, request, obj=None):
        if not obj:
            return []
        else:
            return list(set(
                [field.name for field in self.opts.local_fields] +
                [field.name for field in self.opts.local_many_to_many]
            ))

    def has_add_permission(self, request):
        pass

    def has_delete_permission(self, request, obj=None):
        pass
