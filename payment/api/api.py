from django.conf.urls import url
from order.api.views import OrderViewSet


urlpatterns = [
    url(r'^api/v1/orders/(?P<pk>[0-9]+)/pay/$', OrderViewSet.as_view({'get': 'pay'}),
        name='pay_order'),
]
