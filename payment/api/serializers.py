from rest_framework import serializers

from payment.models import Payment


class PaymentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Payment
        fields = ('id', 'amount', 'created_time', 'payment_status')
        read_only_fields = ('payment_status', 'amount')
