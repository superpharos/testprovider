from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import permissions
from rest_framework.documentation import include_docs_urls
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


schema_view = get_schema_view(
   openapi.Info(
      title="TestExam API Doc",
      default_version='v1',
      description="Doc Detail for Developers",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('testapp.urls'), name='testapp_main_api'),
    url(r'^', include('order.api.api'), name='order_main_api'),
    url(r'^', include('payment.api.api'), name='payment_main_api'),
    url(r'^', include('testtaker.api.api'), name='testtaker_main_api'),
    url(r'^doc/', include_docs_urls(title='TestExam API Doc', public=True, permission_classes=[permissions.AllowAny])),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]
