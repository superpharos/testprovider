from django.conf.urls import url
from order.api.views import OrderViewSet, OrderTestExamViewSet, OrderTestTakerViewSet


urlpatterns = [
    url(r'^api/v1/orders/$', OrderViewSet.as_view({'get': 'list', 'post': 'create'}),
        name='order_list'),
    url(r'^api/v1/orders/(?P<pk>[0-9]+)/$', OrderViewSet.as_view(
        {'get': 'retrieve', 'put': 'update'}),
        name='order_detail'),
    url(r'^api/v1/orders/(?P<order_test_exam_id>[0-9]+)/order-tests/$',
        OrderTestExamViewSet.as_view(
            {'get': 'list', 'post': 'create'}),
        name='order_test_exam_list'),
    url(r'^api/v1/orders/(?P<order_test_exam_id>[0-9]+)/order-tests/(?P<pk>[0-9]+)/$',
        OrderTestExamViewSet.as_view(
            {'get': 'retrieve', 'delete': 'destroy'}),
        name='order_test_exam_detail'),
    url(r'^api/v1/orders/(?P<order_test_exam_id>[0-9]+)/test-takers/$', OrderTestTakerViewSet.as_view({'get': 'list'}),
        name='test_taker_uuid_list'),
    url(r'^api/v1/orders/(?P<order_test_exam_id>[0-9]+)/test-takers/(?P<pk>[0-9A-Fa-f]{8}(-[0-9A-Fa-f]{4}){3}-[0-9A-Fa-f]{12})/$', OrderTestTakerViewSet.as_view({'get': 'retrieve'}),
        name='test_taker_uuid_detail'),
]
