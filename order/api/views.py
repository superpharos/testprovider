from django.db.models import Sum
from django.db import IntegrityError
from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet
from django_filters.rest_framework import DjangoFilterBackend
from django_filters import FilterSet, CharFilter, NumberFilter
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_401_UNAUTHORIZED,
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_204_NO_CONTENT
)
from rest_framework.response import Response

from order.models import Order, OrderTestExam
from payment.models import Payment
from  testtaker.models import TestTaker
from .serializers import OrderSerializer, OrderTestExamSerializer,\
    OrderPostSerializer, OrderTestExamDetailSerializer
from testtaker.api.serializers import TakerTestURISerializer, TakerTestFullDetailSerializer


class OrderFilter(FilterSet):
    total_price = NumberFilter(field_name='total_price', lookup_expr='exact')
    total_price__gt = NumberFilter(field_name='total_price', lookup_expr='gt')
    total_price__lt = NumberFilter(field_name='total_price', lookup_expr='lt')

    class Meta:
        model = Order
        fields = {
            'created_time': ['exact', 'year__gt', 'year__lt', 'month__gt',
                             'month__lt', 'day__gt', 'day__lt'],
        }


class OrderViewSet(ModelViewSet):
    serializer_class = OrderSerializer
    queryset = Order.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filter_class = OrderFilter
    http_method_names = ['get', 'post', 'put']

    action_serializers = {
        'retrieve': OrderSerializer,
        'list': OrderSerializer,
        'create': OrderPostSerializer,
        'pay': OrderSerializer,
        'update': OrderPostSerializer
    }

    def get_serializer_class(self):

        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]

        return super(OrderViewSet, self).get_serializer_class()

    @action(methods=['get', 'pay'], detail=False)
    def pay(self, request, *args, **kwargs):
        test2 = self.get_object()
        test3 = test2.payments.id
        if Payment.objects.get(pk=test3).payment_status:
            json = {'status': 'Already paid'}
            return Response(json, status=HTTP_200_OK)
        pay_done = Payment.objects.get(pk=test3)
        pay_done.payment_status = True
        pay_done.save(update_fields=["payment_status"])
        json = {'status': 'paid'}
        return Response(json, status=HTTP_200_OK)

    def get_queryset(self):
        user = self.request.user
        return Order.objects.filter(user=user).order_by('created_time')

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            json = {'status': 'created'}
            return Response(json, status=HTTP_201_CREATED)

        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        pass


class OrderTestExamViewSet(ModelViewSet):
    serializer_class = OrderTestExamSerializer
    queryset = OrderTestExam.objects.all()
    http_method_names = ['get', 'post', 'put', 'delete']

    def get_serializer_class(self):
        if hasattr(self, 'action'):
            if self.action == 'retrieve':
                detail_one = self.get_object()
                if detail_one.order.payments.payment_status:
                    return OrderTestExamDetailSerializer

                return OrderTestExamSerializer

            return OrderTestExamSerializer

    def get_queryset(self):
        return OrderTestExam.objects.filter(order=self.kwargs['order_test_exam_id'])

    def destroy(self, request, *args, **kwargs):
        order_exam_obj = self.get_object()
        order_exam_id = order_exam_obj.order.id
        order_exam_pay = order_exam_obj.order.payments.id

        if order_exam_obj.order.payments.payment_status:
            return Response(status=HTTP_401_UNAUTHORIZED)

        self.perform_destroy(order_exam_obj)
        total_price = Order.objects.filter(pk=order_exam_id).\
            aggregate(Sum('order_test_exams__total_price'))
        try:
            Order.objects.filter(pk=order_exam_id).update(
                total_price=total_price['order_test_exams__total_price__sum'])
            Payment.objects.filter(pk=order_exam_pay).update(
                amount=total_price['order_test_exams__total_price__sum'])
        except IntegrityError:
            Order.objects.filter(pk=order_exam_id).update(total_price=0)
            Payment.objects.filter(pk=order_exam_pay).update(amount=0)

        return Response(status=HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            serializer.save(order_id=self.kwargs['order_test_exam_id'])

            total_price = Order.objects.filter(
                pk=self.kwargs['order_test_exam_id']).aggregate(
                Sum('order_test_exams__total_price'))
            Order.objects.filter(pk=self.kwargs['order_test_exam_id']).update(
                    total_price=total_price['order_test_exams__total_price__sum'])
            order = Order.objects.get(pk=self.kwargs['order_test_exam_id'])

            order_exam_pay = order.payments.id

            Payment.objects.filter(pk=order_exam_pay).update(
                amount=total_price['order_test_exams__total_price__sum'])
            json = {'status': 'created'}
            return Response(json, status=HTTP_201_CREATED)

        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class OrderTestTakerViewSet(ModelViewSet):
    serializer_class = TakerTestURISerializer
    queryset = TestTaker.objects.all()
    http_method_names = ['get']

    action_serializers = {
        'retrieve': TakerTestFullDetailSerializer,
        'list': TakerTestURISerializer,
    }

    def get_serializer_class(self):

        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]

        return super(OrderTestTakerViewSet, self).get_serializer_class()

    def get_queryset(self):
        return TestTaker.objects.filter(order=self.kwargs['order_test_exam_id'])

