from rest_framework import serializers

from order.models import Order, OrderTestExam
from payment.api.serializers import PaymentSerializer
from payment.models import Payment
from testapp.serializers import TestExamSerializer


class OrderTestExamDetailSerializer(serializers.ModelSerializer):
    test_exam = TestExamSerializer()

    class Meta:
        model = OrderTestExam
        fields = ('id', 'test_exam', 'quantity', 'total_price', 'price_unit')
        read_only_fields = ('total_price', 'price_unit', 'test_exam')


class OrderTestExamSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderTestExam
        fields = ('id', 'test_exam', 'quantity', 'total_price', 'price_unit')
        read_only_fields = ('total_price', 'price_unit')

    def create(self, validated_data):
        quantity = validated_data.pop('quantity')
        test_exam = validated_data.pop('test_exam')
        price = test_exam.price_unit
        total_price = price * quantity
        order_test_exam = OrderTestExam.objects.create(total_price=total_price,
                                                       price_unit=price,
                                                       test_exam_id=test_exam.id,
                                                       quantity=quantity, **validated_data)
        return order_test_exam


class OrderSerializer(serializers.ModelSerializer):
    order_test_exams = OrderTestExamSerializer(many=True)
    payments = PaymentSerializer(required=False)

    class Meta:
        model = Order
        fields = ('id', 'created_time', 'description', 'total_price',
                  'user', 'order_test_exams', 'payments')
        read_only_fields = ('total_price', 'user', 'payments')


class OrderPostSerializer(serializers.ModelSerializer):
    order_test_exams = OrderTestExamSerializer(many=True)

    class Meta:
        model = Order
        fields = ('id', 'created_time', 'description', 'total_price',
                  'user', 'order_test_exams'
                  )
        read_only_fields = ('total_price', 'user')

    def create(self, validated_data):
        order_test_exams = validated_data.pop('order_test_exams', None)
        user = self.context['request'].user
        order = Order.objects.create(user=user, **validated_data)
        order_id = order.id

        if order_test_exams is not None:
            order_total_price = 0
            for each in order_test_exams:
                quantity = each.pop('quantity')
                test_exam = each.pop('test_exam')
                price = test_exam.price_unit
                total_price = price*quantity
                OrderTestExam.objects.create(order=order, total_price=total_price,
                                             price_unit=price, test_exam_id=test_exam.id,
                                             quantity=quantity)
                order_total_price += total_price
            order = Order.objects.filter(pk=order_id).update(total_price=order_total_price)
            Payment.objects.create(order_id=order_id, amount=order_total_price)

        return order

    # def update(self, instance, validated_data):
    #     order_test_exams = validated_data.pop('order_test_exams', None)
    #     order = Order.objects.create(**validated_data)
    #     order_id = order.id
    #
    #     if order_test_exams is not None:
    #         order_total_price = 0
    #         for each in order_test_exams:
    #             quantity = each.pop('quantity')
    #             test_exam = each.pop('test_exam')
    #             price = test_exam.price_unit
    #             total_price = price * quantity
    #             OrderTestExam.objects.create(order=order, total_price=total_price,
    #                                          price_unit=price,
    #                                           test_exam_id=test_exam.id, quantity=quantity)
    #             order_total_price += total_price
    #         order = Order.objects.filter(pk=order_id).update(total_price=order_total_price)
    #
    #     return order


