import json
from django.contrib.auth.models import User
from django.urls import reverse

from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from order.models import Order
from testapp.models import Category, TestExam
from order.api.serializers import OrderSerializer


class OrderListCreateAPIViewTestCase(APITestCase):
    url = reverse("order_list")

    def setUp(self):
        self.username = "arash"
        self.email = "arash@localhost"
        self.password = "arash is valid"
        self.user = User.objects.create_user(self.username, self.email, self.password)
        self.category = Category.objects.auto_created
        self.testexam = TestExam.objects.auto_created
        self.token = Token.objects.create(user=self.user)
        self.api_authentication()

    def api_authentication(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_create_order(self):
        response = self.client.post(self.url,
                                    {"order_test_exams": [{"test_exam": "1",
                                                           "quantity": "33"}]})
        self.assertEqual(201, response.status_code)

    def test_user_order(self):
        """
        Test to verify user order list
        """
        response = self.client.post(self.url,
                                    {"order_test_exams": [{"test_exam": "1",
                                                           "quantity": "33"}]})
        self.assertTrue(len(json.loads(response.content)) == Order.objects.count())


class OrderDetailAPIViewTestCase(APITestCase):

    def setUp(self):
        self.username = "arash"
        self.email = "arash@localhost"
        self.password = "arash is valid"
        self.user = User.objects.create_user(self.username, self.email, self.password)
        self.order = Order.objects.create(user=self.user, total_price=1000)
        self.url = reverse("order_detail", kwargs={"pk": self.order.pk})
        self.token = Token.objects.create(user=self.user)
        self.api_authentication()

    def api_authentication(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_order_object_bundle(self):
        """
        Test to verify order object bundle
        """
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

        order_serializer_data = OrderSerializer(instance=self.order).data
        response_data = json.loads(response.content)
        self.assertEqual(order_serializer_data, response_data)

    def test_order_object_delete_authorization(self):
        """
            Test to verify that put call with different user token
        """
        new_user = User.objects.create_user("newuser", "new@user.com", "newpass")
        new_token = Token.objects.create(user=new_user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + new_token.key)
        response = self.client.delete(self.url)
        self.assertEqual(404, response.status_code)

    def test_order_object_delete(self):
        response = self.client.delete(self.url)
        self.assertEqual(204, response.status_code)
