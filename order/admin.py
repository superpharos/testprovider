from django.contrib import admin

from .models import Order, OrderTestExam


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'total_price', 'created_time')
    list_filter = ('user', 'created_time',)

    def get_readonly_fields(self, request, obj=None):
        if not obj:
            return []
        else:
            return list(set(
                [field.name for field in self.opts.local_fields] +
                [field.name for field in self.opts.local_many_to_many]
            ))

    def has_add_permission(self, request):
        pass

    def has_delete_permission(self, request, obj=None):
        pass


@admin.register(OrderTestExam)
class OrderTestExamAdmin(admin.ModelAdmin):
    list_display = ('id', 'order', 'test_exam', 'price_unit',
                    'quantity', 'total_price'
                    )
    list_filter = ('order', 'test_exam')

    def get_readonly_fields(self, request, obj=None):
        if not obj:
            return []
        else:
            return list(set(
                [field.name for field in self.opts.local_fields] +
                [field.name for field in self.opts.local_many_to_many]
            ))

    def has_add_permission(self, request):
        pass

    def has_delete_permission(self, request, obj=None):
        pass
