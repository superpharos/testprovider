from django.db import models
from django.db.models import PROTECT
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from testapp.models import TestExam


class Order(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             verbose_name=_("order Creator"),
                             on_delete=PROTECT)
    total_price = models.PositiveIntegerField(default=0,
                                              verbose_name=_("Total Price"))
    created_time = models.DateTimeField(auto_now_add=True,
                                        verbose_name=_("created time"))
    description = models.TextField(verbose_name=_("description"),
                                   blank=True, null=True)

    def __str__(self):
        return '%s' % (self.id,)


class OrderTestExam(models.Model):
    order = models.ForeignKey(Order, related_name='order_test_exams',
                              verbose_name=_("orders"))
    test_exam = models.ForeignKey(TestExam, verbose_name=_("Test Exams"),
                                  on_delete=PROTECT)
    price_unit = models.PositiveIntegerField(verbose_name=_("price per unit"))
    total_price = models.PositiveIntegerField(verbose_name=_("total price"))
    quantity = models.PositiveIntegerField(verbose_name=_("quantity"))

    def __str__(self):
        return '%s' % (self.order,)
