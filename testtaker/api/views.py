from rest_framework.viewsets import ModelViewSet
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_401_UNAUTHORIZED,
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_204_NO_CONTENT
)
from rest_framework.response import Response

from .serializers import TakerTestExamSerializer, TakerAnswerPostSerializer
from testtaker.models import TestTaker


class TestTakerViewSet(ModelViewSet):
    queryset = TestTaker.objects.all()
    http_method_names = ['get', 'post']

    action_serializers = {
        'retrieve': TakerTestExamSerializer,
        'create': TakerAnswerPostSerializer,
        'update': None
    }

    def get_serializer_class(self):

        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]

        return super(TestTakerViewSet, self).get_serializer_class()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            serializer.save(test_taker_id=self.kwargs['pk'])
            json = {'status': 'created'}
            return Response(json, status=HTTP_201_CREATED)

        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
