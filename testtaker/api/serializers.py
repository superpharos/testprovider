from rest_framework import serializers
from rest_framework.fields import URLField

from testapp.serializers import TestExamSerializer, TestExamAbstractSerializer, ChoiceAbstractSerializer, QuestionSerializer
from testtaker.models import TestTaker, TestTakerInfo, Answer


class TakerAnswerSerializer(serializers.ModelSerializer):
    choice = ChoiceAbstractSerializer(required=False)
    question = QuestionSerializer(required=False)

    class Meta:
        model = Answer
        fields = ('id', 'question', 'choice')


class TestTakerInfoSerializer(serializers.ModelSerializer):
    taker_answers = TakerAnswerSerializer(many=True)

    class Meta:
        model = TestTakerInfo
        fields = ('id', 'full_name', 'phone_number', 'email', 'taker_answers')


class TakerTestFullDetailSerializer(serializers.ModelSerializer):
    url = URLField(source='get_absolute_url')
    test_exam = TestExamSerializer()
    test_taker_info = TestTakerInfoSerializer()

    class Meta:
        model = TestTaker
        fields = ('id', 'url', 'order', 'created_time', 'test_exam', 'test_taker_info')


class TakerTestURISerializer(serializers.ModelSerializer):
    url = URLField(source='get_absolute_url')
    test_exam = TestExamAbstractSerializer()

    class Meta:
        model = TestTaker
        fields = ('id', 'url', 'test_exam', 'order', 'created_time')


class TakerTestExamSerializer(serializers.ModelSerializer):
    test_exam = TestExamSerializer()

    class Meta:
        model = TestTaker
        fields = ('id', 'test_exam')


class AnswerAbstractSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ('id', 'question', 'choice')


class TakerAnswerPostSerializer(serializers.ModelSerializer):
    post_answers = AnswerAbstractSerializer(many=True)

    class Meta:
        model = TestTakerInfo
        fields = ('id', 'full_name', 'phone_number', 'email', 'post_answers')

    def create(self, validated_data):
        post_answers = validated_data.pop('post_answers', None)
        test_taker_info = TestTakerInfo.objects.create(**validated_data)

        if post_answers is not None:
            for each in post_answers:
                question = each.pop('question')
                choice = each.pop('choice')
                Answer.objects.create(test_taker_info=test_taker_info, question=question, choice=choice)

        return test_taker_info
