from django.conf.urls import url
from .views import TestTakerViewSet


urlpatterns = [
    url(r'^api/v1/test-takers/(?P<pk>[0-9A-Fa-f]{8}(-[0-9A-Fa-f]{4}){3}-[0-9A-Fa-f]{12})/$'
        , TestTakerViewSet.as_view({'get': 'retrieve', 'post': 'create'}), name='test_taker_uuid'),
]
