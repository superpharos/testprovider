from django.contrib import admin

from .models import TestTaker, TestTakerInfo, Answer


@admin.register(TestTaker)
class TestTakerAdmin(admin.ModelAdmin):
    list_display = ('id', 'order', 'test_exam', 'created_time')
    list_filter = ('test_exam', 'created_time')

    def get_readonly_fields(self, request, obj=None):
        if not obj:
            return []
        else:
            return list(set(
                [field.name for field in self.opts.local_fields] +
                [field.name for field in self.opts.local_many_to_many]
            ))

    def has_add_permission(self, request):
        pass

    def has_delete_permission(self, request, obj=None):
        pass


@admin.register(TestTakerInfo)
class TestTakerInfoAdmin(admin.ModelAdmin):
    list_display = ('id', 'full_name', 'phone_number', 'email', 'test_taker', 'created_time')
    list_filter = ('created_time',)

    def get_readonly_fields(self, request, obj=None):
        if not obj:
            return []
        else:
            return list(set(
                [field.name for field in self.opts.local_fields] +
                [field.name for field in self.opts.local_many_to_many]
            ))

    def has_add_permission(self, request):
        pass

    def has_delete_permission(self, request, obj=None):
        pass


@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = ('id', 'test_taker_info', 'question', 'choice')

    def get_readonly_fields(self, request, obj=None):
        if not obj:
            return []
        else:
            return list(set(
                [field.name for field in self.opts.local_fields] +
                [field.name for field in self.opts.local_many_to_many]
            ))

    def has_add_permission(self, request):
        pass

    def has_delete_permission(self, request, obj=None):
        pass
