import uuid as uuid

from django.core.validators import RegexValidator
from django.db import models
from django.db.models import PROTECT
from django.utils.translation import ugettext_lazy as _
from rest_framework.reverse import reverse

from testapp.models import TestExam, Question, Choice
from order.models import Order


class TestTaker(models.Model):
    order = models.ForeignKey(Order, related_name='order_test_takers',
                              on_delete=PROTECT, verbose_name=_("order's Test Taker"))
    test_exam = models.ForeignKey(TestExam, related_name='test_taker_exams',
                                  on_delete=PROTECT, verbose_name=_("Test Taker's TestExam"))
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_time = models.DateTimeField(auto_now_add=True,
                                        verbose_name=_("created time"))

    def get_absolute_url(self):
        kwargs = {'pk': self.id}
        return reverse('test_taker_uuid', kwargs=kwargs)

    def __str__(self):
        return '%s' % (self.id,)


class TestTakerInfo(models.Model):
    full_name = models.CharField(max_length=100, verbose_name=_("Full Name"))
    phone_regex = RegexValidator(regex=r'^\+?[9][8]\d{10}',
                                 message="Phone number must be entered in the format:"
                                         " '+989xxxxxxxxx'. only 12 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=13)
    email = models.EmailField(max_length=254)
    test_taker = models.OneToOneField(TestTaker, related_name='test_taker_info',
                                      verbose_name=_('Test Taker ID'), on_delete=PROTECT)
    created_time = models.DateTimeField(auto_now_add=True,
                                        verbose_name=_("created time"))

    def __str__(self):
        return '%s' % (self.id,)


class Answer(models.Model):
    test_taker_info = models.ForeignKey(TestTakerInfo, related_name='taker_answers')
    question = models.ForeignKey(Question)
    choice = models.ForeignKey(Choice)

    def __str__(self):
        return '%s' % (self.id,)


